from flask import Flask, render_template
from flask_restful import Resource, Api
from controllers import Hello, Square


# creating the flask app
app = Flask(__name__)
# creating an API object
api = Api(app)

# adding the defined resources along with their corresponding urls
api.add_resource(Hello, '/')
api.add_resource(Square, '/square/<int:num>')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)
